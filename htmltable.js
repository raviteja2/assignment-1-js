// Creating a table
(function(){
	const table = document.createElement("table")
	table.setAttribute("id","table-area")
	document.body.appendChild(table)	
})();

// creating table row
(function(){
	let table_row = document.createElement("tr")
document.getElementById('table-area').appendChild(table_row)
})();

// creating table column names
(function(){
	let headings = ['Name','Age','DOB','Email','Company']
	for(let i=0;i<5;i++){
  	    let table_header = document.createElement("th")
		let text = document.createTextNode(headings[i])
		table_header.appendChild(text)
		document.getElementsByTagName('tr')[0].appendChild(table_header)
  }
  document.getElementsByTagName('tr')[0].style.fontWeight = "900";
})();

// creating and adding table data
(function(){
	let arr = [['ravi','21','12-2-2000','ravi@gmail.com','xyz'],['teja','22','01-12-1998','teja@gmail.com','abc'],['eazy','24','10-6-1997','eazy@gmail.com','def'],['arjun','19','1-1-2002','arjun@gmail.com','ghi'],['name','28','12-2-1997','name@gmail.com','jkl'],['name1','24','7-7-1997','name.1@gmail.com','pqr']]
  for(let i=0;i<5;i++){
  	for(let j=0;j<5;j++){
  		let table_data = document.createElement("td")
  		let text = document.createTextNode(arr[i][j])
  		table_data.appendChild(text)
  		let table_row = document.createElement("tr")
  		document.getElementById('table-area').appendChild(table_row)
  		document.getElementsByTagName('tr')[i+1].appendChild(table_data)  	
    }
  }
	
})();